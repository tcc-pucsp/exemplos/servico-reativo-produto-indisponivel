package br.com.projetos.produto.indisponivel.dto.response;

import lombok.Data;

@Data
public class ProdutoDTO {

    private Long id;

    private String nome;

    private Integer quantidadeDisponivel;

    private Integer status;

    private Integer desconto;

    private Float valor;

}
