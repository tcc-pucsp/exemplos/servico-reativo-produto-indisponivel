package br.com.projetos.produto.indisponivel.servico;

import br.com.projetos.produto.indisponivel.cliente.ClienteProduto;
import br.com.projetos.produto.indisponivel.cliente.ClienteProdutoCache;
import br.com.projetos.produto.indisponivel.cliente.ClienteProdutoConcreto;
import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import br.com.projetos.produto.indisponivel.enumeracao.Enumeracao;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.net.ConnectException;

@Service
public class ServicoProduto {

    private final ClienteProduto clienteProduto;

    private final ClienteProdutoCache clienteProdutoCache;

    public ServicoProduto(ClienteProdutoConcreto clienteProduto, ClienteProdutoCache clienteProdutoCache) {
        this.clienteProduto = clienteProduto;
        this.clienteProdutoCache = clienteProdutoCache;
    }

    public Flux<ProdutoDTO> obterProdutosComStatusIgualAtivo() {

        return clienteProduto
                .obterTodosProdutos()
                .onErrorResume(ConnectException.class, e -> clienteProdutoCache.obterTodosProdutos())
                .doOnNext(clienteProdutoCache::inserirProdutoDTO)
                .filter(produto -> Enumeracao.PRODUTO_PENDENTE.igual(produto.getStatus()))
                .switchIfEmpty(clienteProdutoCache::ifVazioEntao);

    }

}
