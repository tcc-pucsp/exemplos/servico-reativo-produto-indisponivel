package br.com.projetos.produto.indisponivel.cliente;

import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import br.com.projetos.produto.indisponivel.propriedade.ProdutoPropriedades;
import org.reactivestreams.Subscriber;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.HashMap;

@Component
public class ClienteProdutoCache implements ClienteProduto {

    private final ProdutoPropriedades propriedades;

    private HashMap<Long, ProdutoDTO> produtos;

    public ClienteProdutoCache(ProdutoPropriedades produtoPropriedades) {
        propriedades = produtoPropriedades;
        produtos = new HashMap<>();
    }

    @Override
    public Flux<ProdutoDTO> obterTodosProdutos() {
        return Flux.fromIterable(produtos.values());
    }

    public void inserirProdutoDTO(ProdutoDTO produtoDTO) {
        if (produtos.size() == propriedades.getCacheLimite())
            produtos.clear();
        produtos.put(produtoDTO.getId(), produtoDTO);
    }

    public void ifVazioEntao(Subscriber<? super ProdutoDTO> subscriber) {
        produtos.clear();
        subscriber.onComplete();
    }

}
