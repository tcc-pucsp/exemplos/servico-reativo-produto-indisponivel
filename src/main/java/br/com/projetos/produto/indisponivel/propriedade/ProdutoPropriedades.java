package br.com.projetos.produto.indisponivel.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "produto")
public class ProdutoPropriedades {

    private String url;

    private Integer cacheLimite;

}
