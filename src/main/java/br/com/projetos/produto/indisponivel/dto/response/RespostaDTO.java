package br.com.projetos.produto.indisponivel.dto.response;

import lombok.Data;

@Data
public class RespostaDTO {

    private Integer codigo;

    private String descricao;

    private String mensagem;

}
