package br.com.projetos.produto.indisponivel;

import br.com.projetos.produto.indisponivel.propriedade.ProdutoPropriedades;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ProdutoPropriedades.class)
public class IndisponivelApplication {

    public static void main(String[] args) {
        SpringApplication.run(IndisponivelApplication.class, args);
    }

}
