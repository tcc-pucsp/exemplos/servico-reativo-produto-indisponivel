package br.com.projetos.produto.indisponivel.cliente;

import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import br.com.projetos.produto.indisponivel.excessao.ProdutoException;
import br.com.projetos.produto.indisponivel.propriedade.ProdutoPropriedades;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class ClienteProdutoConcreto implements ClienteProduto {

    private final ProdutoPropriedades produtoPropriedades;

    private final WebClient webClient;

    public ClienteProdutoConcreto(ProdutoPropriedades produtoPropriedades, WebClient webClient) {
        this.produtoPropriedades = produtoPropriedades;
        this.webClient = webClient;
    }

    @Override
    public Flux<ProdutoDTO> obterTodosProdutos() {

        return webClient
                .get()
                .uri(produtoPropriedades.getUrl())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::responseError)
                .bodyToFlux(ProdutoDTO.class);

    }

    private Mono<? extends Throwable> responseError(ClientResponse clientResponse) {
        throw new ProdutoException(clientResponse);
    }

}
