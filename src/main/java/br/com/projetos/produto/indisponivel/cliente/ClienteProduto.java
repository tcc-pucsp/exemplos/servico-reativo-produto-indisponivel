package br.com.projetos.produto.indisponivel.cliente;

import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import reactor.core.publisher.Flux;

public interface ClienteProduto {

    Flux<ProdutoDTO> obterTodosProdutos();

}
