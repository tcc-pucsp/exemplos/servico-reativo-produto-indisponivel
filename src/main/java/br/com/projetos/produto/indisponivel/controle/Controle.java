package br.com.projetos.produto.indisponivel.controle;

import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import br.com.projetos.produto.indisponivel.servico.ServicoProduto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
public class Controle {

    private final ServicoProduto servico;

    public Controle(ServicoProduto servico) {
        this.servico = servico;
    }

    @GetMapping
    public Flux<ProdutoDTO> obterProdutosComStatusIgualAtivo() {
        return servico.obterProdutosComStatusIgualAtivo();
    }

}
