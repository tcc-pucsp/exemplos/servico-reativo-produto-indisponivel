package br.com.projetos.produto.indisponivel.ferramentas;

import com.google.gson.GsonBuilder;

public interface GsonUtils {

    static String objectToString(Object object) {
        return new GsonBuilder().create().toJson(object);
    }
}
