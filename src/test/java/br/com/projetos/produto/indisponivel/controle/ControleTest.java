package br.com.projetos.produto.indisponivel.controle;

import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import br.com.projetos.produto.indisponivel.dto.response.RespostaDTO;
import br.com.projetos.produto.indisponivel.enumeracao.Enumeracao;
import br.com.projetos.produto.indisponivel.fabrica.ProdutoFabrica;
import br.com.projetos.produto.indisponivel.fabrica.RespostaFabrica;
import br.com.projetos.produto.indisponivel.ferramentas.GsonUtils;
import br.com.projetos.produto.indisponivel.propriedade.ProdutoPropriedades;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.Header;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.LongStream;

import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ControleTest {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ProdutoPropriedades produtoPropriedades;

    private WebTestClient client;

    private ClientAndServer clientAndServer;

    private final String CONTENT_TYPE = "Content-Type";

    private ProdutoDTO martelo;

    private final String METHOD_GET = "GET";

    @Before
    public void setup() {

        final int porta = 1080;

        clientAndServer = new ClientAndServer(porta);

        client = WebTestClient
                .bindToApplicationContext(this.context)
                .configureClient()
                .build();

        martelo = ProdutoFabrica.criar(Enumeracao.PRODUTO_PENDENTE, 1L);

    }

    @Test
    public void obterTodosProdutosComStatusPendente() {

        final ProdutoDTO[] produtos = {
                martelo,
                ProdutoFabrica.criar(Enumeracao.PRODUTO_ATIVO, 2L)
        };

        clientAndServer
                .when(request().withMethod(METHOD_GET))
                .respond(response()
                        .withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(produtos))
                        .withStatusCode(HttpStatus.OK.value()));

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProdutoDTO[].class)
                .isEqualTo(Arrays
                        .stream(produtos)
                        .filter(produto -> Enumeracao.PRODUTO_PENDENTE
                                .igual(produto.getStatus()))
                        .toArray(ProdutoDTO[]::new)
                );

    }

    @Test
    public void obterTodosProdutosComStatusPendentePoremComRetorno4xx() {

        final RespostaDTO respostaDTO = RespostaFabrica.criar();

        clientAndServer
                .when(request().withMethod(METHOD_GET))
                .respond(response()
                        .withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(respostaDTO))
                        .withStatusCode(HttpStatus.FORBIDDEN.value()));

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isForbidden()
                .expectBody(RespostaDTO.class)
                .isEqualTo(respostaDTO);

    }

    @Test
    public void obterTodosProdutosComStatusPendentePoremComServidorVirtualDesligado() {

        clientAndServer.stop();

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProdutoDTO[].class)
                .isEqualTo(Collections.singletonList(martelo).toArray(ProdutoDTO[]::new));

    }

    @Test
    public void obterTodosProdutosComStatusPendentePoremSemRetornoDeColecao() {

        final String jsonSemInformacoes = "[]";

        clientAndServer
                .when(request().withMethod(METHOD_GET))
                .respond(response()
                        .withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(jsonSemInformacoes)
                        .withStatusCode(HttpStatus.OK.value()));

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(jsonSemInformacoes);

    }

    @Test
    public void registarEmMemoriaCacheQuantidadeDeProdutosAcimaDoLimite() {

        List<ProdutoDTO> lista = new ArrayList<>();

        LongStream.range(0, produtoPropriedades.getCacheLimite() + 1).forEach(i -> {
            ProdutoDTO produto = ProdutoFabrica.criar(Enumeracao.PRODUTO_PENDENTE, i);
            lista.add(produto);
        });

        clientAndServer
                .when(request().withMethod(METHOD_GET))
                .respond(response()
                        .withHeader(new Header(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE))
                        .withBody(GsonUtils.objectToString(lista.toArray(ProdutoDTO[]::new)))
                        .withStatusCode(HttpStatus.OK.value()));

        client
                .get()
                .uri("")
                .exchange()
                .expectStatus().isOk()
                .expectBody(ProdutoDTO[].class)
                .isEqualTo(lista.toArray(ProdutoDTO[]::new));

    }

    @After
    public void stopMockServer() {
        clientAndServer.stop();
    }

}
