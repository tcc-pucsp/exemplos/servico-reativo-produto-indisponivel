package br.com.projetos.produto.indisponivel.fabrica;

import br.com.projetos.produto.indisponivel.dto.response.ProdutoDTO;
import br.com.projetos.produto.indisponivel.enumeracao.Enumeracao;

public interface ProdutoFabrica {

    static ProdutoDTO criar(Enumeracao enumeracao, Long id) {

        ProdutoDTO produtoDTO = new ProdutoDTO();

        produtoDTO.setId(id);
        produtoDTO.setDesconto(34);
        produtoDTO.setNome("MARTELO");
        produtoDTO.setStatus(enumeracao.getStatus());
        produtoDTO.setQuantidadeDisponivel(56);
        produtoDTO.setValor(3723f);

        return produtoDTO;

    }
}
