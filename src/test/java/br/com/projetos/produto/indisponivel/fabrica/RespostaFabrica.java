package br.com.projetos.produto.indisponivel.fabrica;

import br.com.projetos.produto.indisponivel.dto.response.RespostaDTO;

public interface RespostaFabrica {

    static RespostaDTO criar() {

        RespostaDTO respostaDTO = new RespostaDTO();

        respostaDTO.setCodigo(1001);
        respostaDTO.setDescricao("Produto não foi encontrado");
        respostaDTO.setMensagem("produto com id '4'");

        return respostaDTO;

    }
}
